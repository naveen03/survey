import {Component, OnInit} from '@angular/core';
import {StateManagerService} from '../../services/state-manager.service';

@Component({
  selector: 'app-summary-template',
  templateUrl: './summary-template.component.html',
  styleUrls: ['./summary-template.component.scss']
})
export class SummaryTemplateComponent implements OnInit {

  summary;

  constructor(private readonly state: StateManagerService) {
  }

  ngOnInit() {
    this.summary = this.state.getSummary();
  }

}
