export class AppState {
  public data;

  constructor() {
    this.data = {};
  }
}

export interface Survey {
  state: string | number;
  label: string;
  nextState?: string | number;
  prevState?: string | number;
  selected?: boolean;
  value?: string;
  submit?: boolean;
}
