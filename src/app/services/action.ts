export class Action {
  type: string;
  payload: any;
}
const AppStateManager = {
  INIT: 'INIT',
  SET: 'SET',
  NEXT: 'NEXT',
  PREV: 'PREV',
  SUBMIT: 'SUBMIT'
};
export const StateActions = {
  APPSTATE: AppStateManager
};
