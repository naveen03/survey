import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {SurveyComponent} from './components/survey/survey.component';
import {FormsModule} from '@angular/forms';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {SummaryTemplateComponent} from './components/summary-template/summary-template.component';
import { RootComponent } from './components/root/root.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SurveyComponent,
    ProgressBarComponent,
    SummaryTemplateComponent,
    RootComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
