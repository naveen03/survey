import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HomeComponent} from './components/home/home.component';
import {SurveyComponent} from './components/survey/survey.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {SummaryTemplateComponent} from './components/summary-template/summary-template.component';
import {RootComponent} from './components/root/root.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, FormsModule
      ],
      declarations: [
        AppComponent, HomeComponent, SurveyComponent, ProgressBarComponent, SummaryTemplateComponent, RootComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'survey'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.ngOnInit).toBeDefined();
    app.ngOnInit();
    expect(app.title).toEqual('Liqid survey');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Liqid survey!');
  });
});
