import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChildren} from '@angular/core';
import {Survey} from '../../helpers/models';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements AfterViewInit {

  @Output() OnChange = new EventEmitter();
  @Input() survey: Survey;
  @ViewChildren('inputRef') inputRef;

  constructor() {
  }

  ngAfterViewInit() {
    if (this.inputRef) {
      console.log('this.inputRef.nativeElement ', this.inputRef.nativeElement);
      setTimeout(() => {
        this.inputRef.first.nativeElement.focus();
      }, 100);
    }
  }


  /**
   * emit parent output fn
   * @param ev: object
   * @param type: string
   */
  handleChange(ev, type) {
    this.OnChange.emit({type, survey: this.survey, ev});
  }

}
