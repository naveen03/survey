import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {AppState, Survey} from '../helpers/models';
import {Action, StateActions} from './action';
import {AppKey} from '../helpers/constants';

@Injectable({
  providedIn: 'root'
})
export class StateManagerService {

  public appState: BehaviorSubject<AppState>;
  initialState: Array<Survey> = [
    {
      state: 1,
      nextState: 2,
      label: 'What is your name'
    },
    {
      state: 2,
      prevState: 1,
      nextState: 3,
      label: 'How old are you'
    },
    {
      state: 3,
      prevState: 2,
      nextState: 4,
      label: ' What is your qualification'
    },
    {
      state: 4,
      prevState: 3,
      nextState: 5,
      label: 'What is your country'
    },
    {
      state: 5,
      prevState: 4,
      label: 'Summary: ',
      submit: true
    }
  ];
  private actions: Array<Action> = [];

  constructor() {
    this.appState = new BehaviorSubject(new AppState());
  }

  /**
   * set appstate
   * @param prevState:
   * @param state:
   * @param key:
   */
  updateAppState(prevState, state, key) {
    this.appState.next({data: {...prevState, [key]: state}});
  }

  /**
   * method to listen all dispatches for state manager
   * @param action: payload of Action type
   */
  dispatch(action: Action) {
    const {type, payload} = action;
    this.actions.push(action);
    switch (type) {
      case StateActions.APPSTATE.INIT: {
        const state = this.appState.value.data;
        this.updateAppState(state, payload, 'survey');
        break;
      }

      case StateActions.APPSTATE.SET: {
        const state = this.appState.value.data;
        const key = 'survey';
        const currState = state && state[key] || {};
        for (const k of currState) {
          k.selected = k.state === payload.state;
        }
        this.updateAppState(state, currState, key);
        break;
      }

      default:
        console.log('Invalid action type: ', type);
    }
  }

  clear() {
    localStorage.clear();
  }

  /**
   * method to return the survey either from storage or initialState variable
   */
  getInitialState() {
    let state;
    try {
      const persistedState = localStorage.getItem(AppKey);
      if (persistedState) {
        state = JSON.parse(persistedState);
      } else {
        state = this.initialState;
      }
    } catch (e) {
      state = this.initialState;
    }
    return state;
  }

  /**
   * method to retieve state
   * @param key:
   */
  getState(key) {
    return this.appState.value.data;
  }

  /**
   * to get summary of survey
   */
  getSummary() {
    const state = this.appState.value.data.survey;
    const summary = [];
    if (state) {
      state.forEach(item => {
        if (!item.submit) {
          summary.push({
            label: item.label,
            value: item.value
          });
        }
      });
    }
    return summary;
  }
}
