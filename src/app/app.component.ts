import {Component, OnInit} from '@angular/core';
import {StateManagerService} from './services/state-manager.service';
import {StateActions} from './services/action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Liqid survey';

  constructor(private readonly state: StateManagerService) {
  }

  ngOnInit() {
    const survey = this.state.getInitialState();
    this.state.dispatch({
      type: StateActions.APPSTATE.INIT,
      payload: survey
    });
  }
}
