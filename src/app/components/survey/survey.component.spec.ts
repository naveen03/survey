import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SurveyComponent} from './survey.component';
import {FormsModule} from '@angular/forms';
import {SummaryTemplateComponent} from '../summary-template/summary-template.component';

describe('SurveyComponent', () => {
  let component: SurveyComponent;
  let fixture: ComponentFixture<SurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [SurveyComponent, SummaryTemplateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyComponent);
    component = fixture.componentInstance;
    component.survey = {state: 1, label: 'Hello'};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have following methods', () => {
    expect(component.ngAfterViewInit).toBeTruthy();
    expect(component.handleChange).toBeTruthy();
  });
});
