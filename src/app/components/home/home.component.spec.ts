import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {SurveyComponent} from '../survey/survey.component';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ProgressBarComponent} from '../progress-bar/progress-bar.component';
import {SummaryTemplateComponent} from '../summary-template/summary-template.component';

class FakeActivatedRoute {
  params = Observable.create(obj => {
    obj.next({id: '123'});
    obj.complete();
  });
}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [HomeComponent, SurveyComponent, ProgressBarComponent, SummaryTemplateComponent],
      providers: [
        {provide: ActivatedRoute, useClass: FakeActivatedRoute},
        {provide: Router, useClass: FakeActivatedRoute},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    component.currentState = {state: 1, label: 'Hello'};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have following methods and props', () => {
    expect(component.currentState).toBeDefined();
    expect(component.id).toBeDefined();
    expect(component.selectedState).toBeDefined();
    expect(component.done).not.toBeDefined();
    expect(component.ngOnInit).toBeTruthy();
    expect(component.handleNext).toBeTruthy();
    expect(component.setCurrentState).toBeTruthy();

    component.setCurrentState([{state: 1}]);
  });
});
