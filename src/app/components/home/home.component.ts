import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StateManagerService} from '../../services/state-manager.service';
import {StateActions} from '../../services/action';
import {Survey} from '../../helpers/models';
import {AppKey} from '../../helpers/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentState: Survey;
  id;
  selectedState = 1;
  doneSurveyPercentage: number;
  done: boolean;
  states;

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly state: StateManagerService) {
  }

  ngOnInit() {
    this.route.params.subscribe(route => {
      this.id = route.id;
      const state = this.state.getState('');
      this.setCurrentState(state.survey);
    });
  }

  /**
   * method to set current state
   * @param survey: Array of survey objects
   */
  setCurrentState(survey) {
    const totalStates = survey.length;
    this.states = survey;
    survey.forEach((item, i) => {
      if (item.selected || item.state.toString() === this.id) {
        this.doneSurveyPercentage = (i + 1) / totalStates * 100;
        this.currentState = item;
      }
    });
    if (localStorage) {
      localStorage.setItem(AppKey, JSON.stringify(survey));
    }
  }


  /**
   * method to dispatch action based on type called
   * @param type: string
   * @param survey: object
   */
  handleNext({type, survey}) {
    const payload: any = {};
    let isDone;
    if (type === StateActions.APPSTATE.NEXT) {
      payload.state = survey.nextState;
    } else if (type === StateActions.APPSTATE.PREV) {
      payload.state = survey.prevState;
    } else if (type === StateActions.APPSTATE.SUBMIT) {
      isDone = true;
      this.done = true;
    }
    if (!isDone) {
      this.router.navigate([`/home/${payload.state}`]);
    }
  }

  /**
   * handle local action based on type
   * @param type: string
   */
  handleAction(type) {
    this.done = false;
    if (type !== 'cancel') {
      this.reset();
    }
  }

  /**
   * reset survey and redirect to home
   */
  reset() {
    this.state.clear();
    this.router.navigate(['/start']);
  }
}
